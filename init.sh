#!/bin/bash

echo "---"
cat variable.env
echo "---"
echo ''
read -p "Is the variables updated in above file? [y/n] " -n 1 -r
echo ''
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Update the variables in ./variable.env file"
    exit 1
fi

#GitPAT=`cat variable.env | grep "GitPAT" | sed 's/^[^=]*=//'`
vol="flogile-ci-volume"
mkdir $vol >& /dev/null

docker login registry.gitlab.com -u flogile -p Tcm2Wu_z9yh2ZCXDKeUi >& /dev/null # -u <deploy_username> -p <deploy_tokens>
docker pull -q registry.gitlab.com/flogile_devops/flogile-ci-demo:ci-vol
sleep 1
docker run -d --name ci-vol registry.gitlab.com/flogile_devops/flogile-ci-demo:ci-vol >& /dev/null
docker logout >& /dev/null

if [ "$#" -ge 1 ]; then
        SERVICEUP=`echo $@`
else
        SERVICEUP=`docker-compose --env-file variable.env config --service`
fi

for SERVICE in $SERVICEUP
do
        HostIp=`cat variable.env | grep -i 'HostIp' | sed 's/.*=//g'`

        if [[ "$SERVICE" == "jenkins" ]];then
                JenkinsPort=`cat variable.env | grep -i 'JenkinsPort' | sed 's/.*=//g'`

                docker cp ci-vol:/ci-vol/JENKINS_HOME $vol/JENKINS_HOME

        fi

        if [[ "$SERVICE" == "nexus" ]];then
                NexusPort=`cat variable.env | grep -i 'NexusPort' | sed 's/.*=//g'`

                docker cp ci-vol:/ci-vol/NEXUS_DATA $vol/NEXUS_DATA

        fi
	
	if [[ "$SERVICE" == "sonarqube" || "$SERVICE" == "sonardb" ]];then
                sysctl -w vm.max_map_count=524288 > /dev/null
                sysctl -w fs.file-max=131072 > /dev/null
                ulimit -n 131072
                ulimit -u 8192

                SonarQPort=`cat variable.env | grep -i 'SonarQPort' | sed 's/.*=//g'`

		## docker cp ci-vol:/ci-vol/SONAR_DATA $vol/SONAR_DATA && docker cp ci-vol:/ci-vol/SONARDB_DATA $vol/SONARDB_DATA

        fi

        if [[ "$SERVICE" == "gitlab" ]];then
                GitLabPort=`cat variable.env | grep -i 'GitLabPort' | sed 's/.*=//g'`

                mkdir -p flogile-ci-volume/GITLAB_CONFIG flogile-ci-volume/GITLAB_DATA
        fi
done

docker rm -f ci-vol >& /dev/null
chmod -R 777 $vol

docker-compose --env-file variable.env up --quiet-pull -d $@

echo ''

JNSun=admin
JNSpwd=demoroot

NUM=1

if [[ `docker-compose ps | grep jenkins | wc -l` -ge $NUM ]];then
        echo '---'
        echo "Jenkins URL : http://${HostIp}:${JenkinsPort:-9080}/jenkins"
        echo "UserName : $JNSun & Passwd : $JNSpwd"
        echo ''
fi

if [[ `docker-compose ps | grep nexus | wc -l` -ge $NUM ]];then
        echo '---'
        echo "Nexus URL : http://${HostIp}:${NexusPort:-9081}/nexus"
        echo "UserName : $JNSun & Passwd : $JNSpwd"
        echo ''
fi

if [[ `docker-compose ps | grep sonarqube | wc -l` -ge $NUM ]];then
        echo '---'
        echo "SonarQube URL : http://${HostIp}:${SonarQPort:-9082}/sonarqube"
        echo "UserName : $JNSun & Passwd : $JNSun"        
	echo ''
fi

if [[ `docker-compose ps | grep gitlab | wc -l` -ge $NUM ]];then
        echo '---'
        sleep 15
        GitLab_PWD=`docker exec demo-my-gitlab cat /etc/gitlab/initial_root_password | grep -i "password:" | sed 's/.* //g'`
        echo "GitLab URL : http://${HostIp}:${GitLabPort:-9086}/gitlab"
        echo "GitLab UserName: root"
        echo "GitLab Passwd: ${GitLab_PWD}"
        echo ''
fi
