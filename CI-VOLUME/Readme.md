INSTRUCTIONS
------------

The Below listed Directory are the preholds the Volume-data  
```
./JENKINS_HOME/*	-	is the Directory that has Volume-data of Jenkins.  
./NEXUS_DATA/*		-	is the Directory that has Volume-data of Nexus.  

`./<ANYNEW>/*`		-	if you want to add any new Volume-data directory to it, add the directory of volume in a directory name as 
					servicename_data(note it for later use).  
```

`Dockerfile`	-	to containerize the above data which will be used in initilize section  


To containerize the above data use the below command  
```sh
$ docker build -t registry.gitlab.com/flogile_devops/flogile-ci-demo:ci-vol .
```

To push the above image to registry  
	Go to gitlab.com/flogile_devops/ -> container_registry -> del "ci-vol" tag (previous tag)  
	
```sh
$ docker login registry.gitlab.com -u flogile_devops -p @flogilegitlab  
$ docker push registry.gitlab.com/flogile_devops/flogile-ci-demo:ci-vol
```



User and Password for services
------------------------------

<details><summary> Username and Password </summary>

- **Gitlab**
```
USERNAME : root
PASSWORD : defualt_Pwd_will_be shown
```

- **Jenkins, Nexus**
```
USERNAME : admin
PASSWORD : demoroot
```

- **Sonarqube**
```
USERNAME : admin
PASSWORD : admin
```
</details>
