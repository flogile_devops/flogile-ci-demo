# Flogile-CI-Demo
This Repo contains the deployment configuration and scripts for the (CI - Continuous Integration) CI demo process, here we are going to deploy Jenkins, Nexus, and SonarQube.


## Pre-requestries:
- Install [Docker](https://docs.docker.com/get-docker/) and [Docker-compose](https://docs.docker.com/compose/install/)
- Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Allow the firewall ports that you want to expose the services. check the "Defualt port configuration" section to know where the port exposed by defualt.  

#### Defualt port configuration:

| Service_name  | Expose_ports  | Constant_ports    |
| :---:         | :---:         | :---:             |
| Jenkins       | 9080          | -                 |
| Nexus         | 9081          | -                 |
| Sonar         | 9082          | 9083              |
| SonarDb       | -             | 5432              |
| Gitlab        | 9086          | 9087 & 9088       |

    Note:
    - Expose_ports are the ports where service is exposed on that respective port. (Can be change by passing variable)  
    - Constant_ports are the ports where service use for other relevant purpose. (Cannot be change by passing variable) if you want to change edit it in the compose file.

## How to use:
- Clone the repo where you want to deploy the services and move in to the dir.  
    ```bash
    $ git clone <CLONE_URL> 
    $ cd flogile-ci-demo
    ```  
- Update the variable file which is variable.env.

| Var_Names     |   Defualt_value   |   Explanation                                                 |
| :---:         |   :---:           |   ---:                                                        |
| HostIp        |   -               |   Host machine's IP                                           |
| JenkinsPort   |   9080            |   can be overwrite with new port to run Jenkins the host      |
| NexusPort     |   9081            |   can be overwrite with new port to run Nexus on the host     |
| NexusCpuLimit |   4               |   can be overwrite with new value of range 4 <= NexusCpuLimit |
| SonarQPort    |   9082            |   can be overwrite with new port to run Sonar on the host     |
| SonarPwd      |   -               |   Password for SonarQubeDB                                    |
| GitLabPort    |   9086            |   can be overwrite with new port to run Gitlab on the host    |

    Note:
    - If Defualt is mentioned or the variable is assigned to empty in variable.env file, then it is a non-mandatory variable.
    - If Defualt is not mentioned or the variable is assigned to Double hash(##) in variable.env file, then it is a mandatory variable.

- After that execute the script to deploy the service
    ```bash
    $ chmod 777 init.sh
    $ ./init.sh
    ```
    Once the script (./init.sh) is executed then it asks to wheather the variable file is updated or not? type [Y/n]  
    Then it deploys the services and gives the URL of the services.

    ![Output](/.readme/Output.png "output of above init.sh file")

    You can also run the specific services by executing the script `init.sh` with specfied service_name as below  
    
    ```
    $ ./init service_name-1 service_name-2 service_name-N
    ```
    
    Service_names:
    - jenkins
    - nexus
    - sonarqube/sonardb
    - gitlab

## Volume Storage:-  
- Jenkins  
    `./flogile-ci-volume/JENKINS_HOME:/var/jenkins_home`
- Nexus  
    `./flogile-ci-volume/NEXUS_DATA:/nexus-data`
- SonarQube  
    `./flogile-ci-volume/SONAR_DATA:/opt/sonarqube/data`
- SonarQubeDb  
    `./flogile-ci-volume/SONARDB_DATA:/var/lib/postgresql/data`
- GitLab  
    `./flogile-ci-volume/GITLAB_CONFIG:/etc/gitlab`
    `./flogile-ci-volume/GITLAB_DATA:/var/opt/gitlab`

## NOTE:
    - Here only jenkins and nexus has the pre volume-data. 

----
----

## Usage:
- This repo has docker-compose file which will be used for the deploy the services on the basis of environment variable in variable.env file.  
- And the init.sh bash script to deploy the required or all services and merge the preloaded volume data of services.  
- The preloaded volume data is equiped in the docker-image in gitlab container registry.(further instruction in CI-Volume readme file) 
